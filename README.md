Microsoft DevOps FastTrack
==========================

A környezet
-----------

Érdemes egy python virtualenvet csinálni, és abba felrakni a szükséges python
utility-ket. (ansible, azure-cli).

```
$ mkdir -p .venv
$ virtualenv .venv/azure
$ source .venv/azure/bin/activate
$ pip install --upgrade pip
$ pip install -r requirements.txt
```

Be kell jenlentkezni azure-ba:

```
$ az login
To sign in, use a web browser to open the page https://aka.ms/devicelogin and enter the code CQ8ZERFVH to authenticate.
[
  {
    "cloudName": "AzureCloud",
    "id": "361c4107-edef-40c4-a718-9062be971528",
    "isDefault": true,
    "name": "Pay-As-You-Go",
    "state": "Enabled",
    "tenantId": "f4619236-a872-4f8f-b17c-01271417dcc4",
    "user": {
      "name": "neilus@elte.hu",
      "type": "user"
    }
  },
  {
    "cloudName": "AzureCloud",
    "id": "5c5a7ccf-09af-4e91-9627-c77a7507bb31",
    "isDefault": false,
    "name": "Free Trial",
    "state": "Enabled",
    "tenantId": "f4619236-a872-4f8f-b17c-01271417dcc4",
    "user": {
      "name": "neilus@elte.hu",
      "type": "user"
    }
  }
]
```

Ha több előfizetése van az embernek (mint nekem) akkor be kell állítani azt,
hogy melyiket használjuk most:

```
$ az account set 5c5a7ccf-09af-4e91-9627-c77a7507bb31
```

Ez után érdemes egy új resource group-ot létrehozni azure-ban:

```
$ az group create -n azure-devops -l westeurope
{
  "id": "/subscriptions/5c5a7ccf-09af-4e91-9627-c77a7507bb31/resourceGroups/azure-devops",
  "location": "westeurope",
  "managedBy": null,
  "name": "azure-devops",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null
}
```

Ekkor elindíthatjuk az első VM-ünket:

```
$ az vm create --image OpenLogic:CentOS:7.2:latest --admin-username devopscourse --ssh-key-value ~/.ssh/id_rsa.pub --public-ip-address-dns-name devopscourse-neilus01 --resource-group azure-devops --location westeurope --size=Standard_A1 --storage-sku Standard_LRS --name ansible01
{
  "fqdns": "devopscourse-neilus01.westeurope.cloudapp.azure.com",
  "id": "/subscriptions/5c5a7ccf-09af-4e91-9627-c77a7507bb31/resourceGroups/azure-devops/providers/Microsoft.Compute/virtualMachines/ansible01",
  "location": "westeurope",
  "macAddress": "00-0D-3A-29-EA-9F",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.4",
  "publicIpAddress": "13.94.154.251",
  "resourceGroup": "azure-devops"
}
```

