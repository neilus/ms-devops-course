#! /bin/bash
az network nsg rule create --resource-group azure-devops \
    --nsg-name ansible01NSG --name devopscourse-http1 \
    --protocol tcp --direction inbound --priority 1001 \
    --source-address-prefix '*' --source-port-range '*' \
    --destination-address-prefix '*' --destination-port-range 80 --access allow
az network nsg rule create --resource-group azure-devops \
    --nsg-name ansible01NSG --name devopscourse-http2 \
    --protocol tcp --direction inbound --priority 1002 \
    --source-address-prefix '*' --source-port-range '*' \
    --destination-address-prefix '*' --destination-port-range 8080 --access allow
az network nsg rule create --resource-group azure-devops \
    --nsg-name ansible01NSG --name devopscourse-http3 \
    --protocol tcp --direction inbound --priority 1003 \
    --source-address-prefix '*' --source-port-range '*' \
    --destination-address-prefix '*' --destination-port-range 8081 --access allow
